# Direct link of videos

## support websites

youtube

instagram

## Demo

### youtube:

[https://imyz.tw/api/youtube?url=https://www.youtube.com/watch?v=tCXGJQYZ9JA](https://imyz.tw/api/youtube?url=https://www.youtube.com/watch?v=tCXGJQYZ9JA)

alternative:

https://imyz.tw/api/youtube?v=tCXGJQYZ9JA

### instagram:

[https://imyz.tw/api/instagram?url=https://www.instagram.com/p/BhZ3JQZB3-y/](https://imyz.tw/api/instagram?url=https://www.instagram.com/p/BhZ3JQZB3-y/)

